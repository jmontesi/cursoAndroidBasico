package com.everis.course.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.everis.course.R;
import com.everis.course.model.Movement;

import java.util.List;

/**
 * Desc:
 * Author: jsancheh
 * Version: 1.0
 */

public class MovementsAdapter extends RecyclerView.Adapter<MovementsAdapter.ViewHolder> {

    private List<Movement> mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private TextView tvExpend;
        private TextView tvDate;
        private TextView tvDesc;

        public ViewHolder(View v) {
            super(v);

            //inject views
            tvTitle = v.findViewById(R.id.tvTitle);
            tvDate = v.findViewById(R.id.tvDate);
            tvExpend = v.findViewById(R.id.tvExpend);
            tvDesc = v.findViewById(R.id.tvDesc);
        }
    }

    public MovementsAdapter(List<Movement> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public MovementsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lay_movement_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Movement movement = mDataset.get(position);
        holder.tvTitle.setText(movement.getType());
        holder.tvDate.setText(movement.getDate());
        holder.tvDesc.setText(movement.getTitle());
        holder.tvExpend.setText(new StringBuilder(movement.getAmount().toString()).append("€"));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
