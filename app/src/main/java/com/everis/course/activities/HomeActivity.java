package com.everis.course.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.everis.course.R;
import com.everis.course.adapters.MovementsAdapter;
import com.everis.course.model.Card;
import com.everis.course.model.CardResponse;
import com.everis.course.model.Movement;
import com.everis.course.network.ApiClient;
import com.everis.course.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Desc:
 * Author: jsancheh
 * Version: 1.0
 */

public class HomeActivity extends AppCompatActivity {


    private TextView tvTotal;
    private TextView tvCardNumber;
    private TextView tvCreditLine;
    private RecyclerView rvMovements;
    private List<Movement> mDataSet;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //instance view
        rvMovements = findViewById(R.id.rvMovements);
        rvMovements.setLayoutManager(new LinearLayoutManager(this));

        tvTotal = findViewById(R.id.tvTotal);
        tvCardNumber = findViewById(R.id.tvCardNumber);
        tvCreditLine = findViewById(R.id.tvCreditLine);

        //instnace dataset
        mDataSet = new ArrayList<>();

        //instance api
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<CardResponse> call = apiService.getCard();
        call.enqueue(new Callback<CardResponse>() {
            @Override
            public void onResponse(Call<CardResponse> call, Response<CardResponse> response) {

                if (response.isSuccessful()){
                    initView(response.body().getCard());
                }
            }

            @Override
            public void onFailure(Call<CardResponse> call, Throwable t) {

                //TODO: Mostraremos un error o un empty space...
                Log.d("","");
            }
        });
    }

    private void initView(Card card){

        //set data
        tvTotal.setText(new StringBuilder(card.getConsumed().toString()).append("€"));
        tvCardNumber.setText(card.getNumber());
        tvCreditLine.setText(new StringBuilder(card.getCreditLine().toString()).append("€"));

        mDataSet.addAll(card.getMovements());

        MovementsAdapter adapter = new MovementsAdapter(mDataSet);
        adapter.notifyDataSetChanged();

        rvMovements.setAdapter(adapter);

    }

}
