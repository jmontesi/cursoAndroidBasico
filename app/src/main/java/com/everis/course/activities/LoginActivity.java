package com.everis.course.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.everis.course.R;
import com.everis.course.fragments.LoginFragment;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //login fragment added
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.llContent, LoginFragment.newInstance(), LoginFragment.TAG)
                .commit();
    }
}
