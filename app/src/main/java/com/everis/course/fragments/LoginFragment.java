package com.everis.course.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.everis.course.R;
import com.everis.course.activities.HomeActivity;
import com.everis.course.model.UserResponse;
import com.everis.course.network.ApiClient;
import com.everis.course.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Desc:
 * Author: jsancheh
 * Version: 1.0
 */

public class LoginFragment extends Fragment implements TextWatcher, View.OnClickListener {

    private EditText etUser;
    private EditText etPass;
    private Button bLogin;
    private String hola = "hola";

    public static final String TAG = LoginFragment.class.getName();

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //iniciamos
        etUser = view.findViewById(R.id.etUser);
        etUser.addTextChangedListener(this);

        etPass = view.findViewById(R.id.etPass);
        etPass.addTextChangedListener(this);

        bLogin = view.findViewById(R.id.bLogin);
        bLogin.setOnClickListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        //nothing
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        bLogin.setEnabled(!etUser.getText().toString().isEmpty() && !etPass.getText().toString().isEmpty());
    }

    @Override
    public void afterTextChanged(Editable editable) {
        //nothing
    }

    @Override
    public void onClick(View view) {

        //instance api
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<UserResponse> call = apiService.getUser();
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                //go home

                if(response.isSuccessful()){

                    Intent intent = new Intent(getContext(), HomeActivity.class);
                    intent.putExtra("USER", response.body().getUser());
                    startActivity(intent);
                }

            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                //show error
            }
        });
    }
}
