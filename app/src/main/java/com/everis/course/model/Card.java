package com.everis.course.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Desc:
 * Author: jsancheh
 * Version: 1.0
 */

public class Card implements Parcelable {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("creditLine")
    @Expose
    private Integer creditLine;
    @SerializedName("consumed")
    @Expose
    private Double consumed;
    @SerializedName("movements")
    @Expose
    private List<Movement> movements = null;

    protected Card(Parcel in) {
        number = in.readString();
        if (in.readByte() == 0) {
            creditLine = null;
        } else {
            creditLine = in.readInt();
        }
        if (in.readByte() == 0) {
            consumed = null;
        } else {
            consumed = in.readDouble();
        }
        movements = in.createTypedArrayList(Movement.CREATOR);
    }

    public static final Creator<Card> CREATOR = new Creator<Card>() {
        @Override
        public Card createFromParcel(Parcel in) {
            return new Card(in);
        }

        @Override
        public Card[] newArray(int size) {
            return new Card[size];
        }
    };

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getCreditLine() {
        return creditLine;
    }

    public void setCreditLine(Integer creditLine) {
        this.creditLine = creditLine;
    }

    public Double getConsumed() {
        return consumed;
    }

    public void setConsumed(Double consumed) {
        this.consumed = consumed;
    }

    public List<Movement> getMovements() {
        return movements;
    }

    public void setMovements(List<Movement> movements) {
        this.movements = movements;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(number);
        if (creditLine == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(creditLine);
        }
        if (consumed == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(consumed);
        }
        parcel.writeTypedList(movements);
    }
}
