package com.everis.course.network;

import com.everis.course.model.CardResponse;
import com.everis.course.model.UserResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Desc:
 * Author: jsancheh
 * Version: 1.0
 */

public interface ApiInterface {


    /**
     * Meétodo para obtener los datos del usuario
     * @return - response data
     */
    @GET("bins/qppwd")
    Call<UserResponse> getUser();

    /**
     * Meétodo para obtener los datos del usuario
     * @return - response data
     */
    @GET("bins/62en1")
    Call<CardResponse> getCard();
}
